#!/usr/bin/env bash
#
# Bash Bootstrap Example Script
#
# Usage:
#
#  LOG_LEVEL=7 ./example.sh [-n|--no-color] [-d|--debug] [-v] [-h|--help]
#
# Based on BASH3 Boilerplate v2.3.0 [https://github.com/kvz/bash3boilerplate]
#
# MIT License
# Copyright (c) 2017 Matthew E. Helm
#

# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Define the environment variables (and their defaults) that this script depends on
__debug=0
__verbose=0
__args=()

# shellcheck disable=SC2015
[[ "${__usage+x}" ]] || {
  read -r -d '' __usage <<EOD || true
  -n --no-color Disable color output
  -d --debug    Enables debug mode
  -v            Enable verbose mode, print script as it is executed
  -h --help     This page
EOD
}

# shellcheck disable=SC2015
[[ "${__helptext+x}" ]] || {
  read -r -d '' __helptext <<EOD || true
 Help Text
EOD
}

# shellcheck source=bootstrap.sh
source "$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/bootstrap.sh"


#
# Parse commandline options
#

while [[ ${#} -gt 0 ]]; do
  case "${1}" in
    '-n'|'--no-color') __color_enabled=0        ;;
    '-d'|'--debug')    __debug=1                ;;
    '-v')              __verbose=1              ;;
    '-h'|'--help')     __help "Help using ${0}" ;;
    *)                 __args+=("${1}")         ;;
  esac; shift
done


#
# Signal trapping and backtracing
#

function __exit_handle () { :; }

# trap __exit_handle EXIT

function __err_handle () {
    local __error_code=${?}
    __error "Error in ${__file} in function ${1} on line ${2}"
    exit ${__error_code}
}

# trap '__err_handle "${FUNCNAME:-.}" ${LINENO}' ERR


#
# Command-line argument switches
#
#  like -d for debugmode, -v for verbose
#

# debug mode
if [[ ${__debug} == 1 ]]; then
  set -o xtrace

  __log_level=7

  # Enable error backtracing
  trap '__err_handle "${FUNCNAME:-.}" ${LINENO}' ERR
fi

# verbose mode
if [[ ${__verbose} == 1 ]]; then
  set -o verbose
fi


#
# Validation
#
#  Error out if the things required for your script are not present
#

[[ "${__log_level:-}" ]] || __emergency "Cannot continue without log level."


#
# Runtime
#

__debug 'Debug message'
__info 'Info message'
__notice 'Notice message'
__warning 'Warning message'
__error 'Error message'
__critical 'Critical message'
__alert 'Alert message'
__emergency 'Emergency message'
