#!/usr/bin/env bash
#
# Bash Bootstrap Script
#
# Usage:
#
#  source "$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/bootstrap.sh"
#
# Based on BASH3 Boilerplate v2.3.0 [https://github.com/kvz/bash3boilerplate]
#
# MIT License
# Copyright (c) 2017 Matthew E. Helm
#

# Exit on error. Append "|| true" if you expect an error.
set -o errexit

# Exit on error inside any functions or subshells.
set -o errtrace

# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset

# Catch the error in case something like mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
set -o pipefail

if [[ "${BASH_SOURCE[0]}" != "${0}" && "${__usage+x}" ]]; then
  __bash_source_index=1
else
  __bash_source_index=0
fi

# Set magic variables for current file, directory, os, etc.
__dir="$(cd "$(dirname "${BASH_SOURCE[${__bash_source_index}]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[${__bash_source_index}]}")"
__base="$(basename "${__file}" .sh)"
__invocation="$(printf %q "${__file}")$(((${#})) && printf ' %q' "${@}" || true)"

if [[ "${TIMESTAMP_FORMAT:-}" ]]; then
  __timestamp_format="${TIMESTAMP_FORMAT}"
else
  __timestamp_format='+%F %T %Z'
fi

__log_level="${LOG_LEVEL:-6}" # 7 = debug -> 0 = emergency
__log_file="${LOG_FILE:-/dev/null}"
__log_file_level="${LOG_FILE_LEVEL:-7}"

if [[ ${FORCE_COLOR:-0} == 1 ]]; then
  __color_enabled=1
elif [[ ${NO_COLOR:-0} != 1 ]]; then
  __available_colors=$(tput colors 2> /dev/null || true)

  if [[ ${__available_colors:=0} -ge 16 && -t 1 ]]; then
    __color_enabled=1
  fi
else
  __color_enabled=0
fi

if [[ ${__color_enabled} == 1 ]]; then
  __esc="$(printf '\e')"

  __color_fg_red="${__esc}[31m"
  __color_fg_green="${__esc}[32m"
  __color_fg_yellow="${__esc}[33m"
  __color_fg_blue="${__esc}[34m"
  __color_fg_magenta="${__esc}[35m"

  __color_bg_red="${__esc}[41m"

  __color_bold="${__esc}[1m"
  __color_underline="${__esc}[4m"
  __color_blink="${__esc}[5m"

  __color_reset="${__esc}[0m"

  __log_style_emergency="${__color_bold}${__color_underline}${__color_blink}${__color_fg_yellow}${__color_bg_red}"
  __log_style_alert="${__color_bold}${__color_fg_yellow}${__color_bg_red}"
  __log_style_critical="${__color_bold}${__color_fg_red}"
  __log_style_error="${__color_fg_red}"
  __log_style_warning="${__color_fg_yellow}"
  __log_style_notice="${__color_fg_blue}"
  __log_style_info="${__color_fg_green}"
  __log_style_debug="${__color_fg_magenta}"
fi

__log_level_emergency=0
__log_level_alert=1
__log_level_critical=2
__log_level_error=3
__log_level_warning=4
__log_level_notice=5
__log_level_info=6
__log_level_debug=7


#
# Functions
#

function __timestamp () {
  date -u "${1:-${__timestamp_format}}"
}

function __log_level_check () {
  local __level_var="__log_level_${1}"

  [[ ${!__level_var} -le ${__log_level} ]]
}

function __log_file_level_check () {
  local __level_var="__log_level_${1}"

  [[ ${!__level_var} -le ${__log_file_level} && "${__log_file}" != '/dev/null' ]]
}

function __log () {
  local __level="${1}"; shift

  if ! __log_level_check "${__level}" && ! __log_file_level_check "${__level}"; then
    return
  fi

  local __level_format='%9s'

  if [[ ${__color_enabled} == 1 ]]; then
    local __style_var="__log_style_${__level}"

    __level_format="${!__style_var}${__level_format}${__color_reset}"
  fi

  {
    if [[ ${#} -gt 0 ]]; then
      printf '%s\n' "${@}"
    else
      cat
    fi
  } | {
    local __line

    while IFS=$'\n' read -r __line; do
      printf "[%s] [${__level_format}] %s\n" "$(__timestamp)" "${__level}" "${__line}"
    done
  } | {
    if __log_level_check "${__level}" && __log_file_level_check "${__level}"; then
      cat > >(tee -a "${__log_file}" >&2)
    elif __log_level_check "${__level}"; then
      cat >&2
    elif __log_file_level_check "${__level}"; then
      cat >> "${__log_file}"
    else
      cat > /dev/null
    fi
  }
}

function __emergency () { __log emergency "${@:-}"; exit 1; }
function __alert ()     { __log alert     "${@:-}"; }
function __critical ()  { __log critical  "${@:-}"; }
function __error ()     { __log error     "${@:-}"; }
function __warning ()   { __log warning   "${@:-}"; }
function __notice ()    { __log notice    "${@:-}"; }
function __info ()      { __log info      "${@:-}"; }
function __debug ()     { __log debug     "${@:-}"; }

function __help () {
  cat >&2 <<EOD

 ${*}

  ${__usage:-No usage available}

EOD

  if [[ "${__helptext:-}" ]]; then
    cat >&2 <<EOD
 ${__helptext}

EOD
  fi

  exit 1
}

unset -v __bash_source_index
